package luckyclient.remote.entity;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

public class ProjectSuite extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /** 鑱氬悎璁″垝ID */
    private Integer suiteId;
    /** 鑱氬悎璁″垝鍚嶇О */
    private String suiteName;
    /** 鑱氬悎涓殑璁″垝鎬绘暟 */
    private Integer suitePlanCount;
    /** 椤圭洰ID */
    private Integer projectId;

    private String createBy;
    /** 鍒涘缓鏃堕棿 */
    private Date createTime;

    private String updateBy;
    /** 鏇存柊鏃堕棿 */
    private Date updateTime;
    /** 澶囨敞 */
    private String remark;
    /** 鍏宠仈椤圭洰瀹炰綋 */
    private Project project;

    public Integer getSuiteId() {
        return suiteId;
    }

    public void setSuiteId(Integer suiteId) {
        this.suiteId = suiteId;
    }

    public String getSuiteName() {
        return suiteName;
    }

    public void setSuiteName(String suiteName) {
        this.suiteName = suiteName;
    }

    public Integer getSuitePlanCount() {
        return suitePlanCount;
    }

    public void setSuitePlanCount(Integer suitePlanCount) {
        this.suitePlanCount = suitePlanCount;
    }

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public String toString(){
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("suiteId", getSuiteId())
                .append("suiteName", getSuiteName())
                .append("suitePlanCount", getSuitePlanCount())
                .append("projectId", getProjectId())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("remark", getRemark())
                .append("project", getProject())
                .toString();
    }

}

