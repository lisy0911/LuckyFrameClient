package luckyclient.utils.log;

import java.lang.annotation.*;


@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Documented
public @interface ApiLog {

    /**
     * 鎻忚堪淇℃伅
     * @return
     */
    String description() default "";

}
