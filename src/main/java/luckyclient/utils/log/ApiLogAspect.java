package luckyclient.utils.log;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Enumeration;

@Aspect
@Component
public class ApiLogAspect {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 鎹㈣绗�
     */
    private static final String LINE_SEPARATOR = System.lineSeparator();

    /**
     * 浠ヨ嚜瀹氫箟 @ApiLog 娉ㄨВ涓哄垏鐐�
     */
    @Pointcut("@annotation(luckyclient.utils.log.ApiLog)")
    public void apiLog() {

    }

    /**
     * 鍦ㄥ垏鐐逛箣鍓嶇粐鍏�
     * @param joinPoint
     * @throws Throwable
     */
    @Before("apiLog()")
    public void doBefore(JoinPoint joinPoint) throws Throwable {
        // 寮�濮嬫墦鍗拌姹傛棩蹇�
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        // 鑾峰彇 @ApiLog 娉ㄨВ鐨勬弿杩颁俊鎭�
        String methodDescription = getAspectLogDescription(joinPoint);
        // 鎵撳嵃璇锋眰鐩稿叧鍙傛暟
        logger.info("========================================== Start ==========================================");
        // 鎵撳嵃璇锋眰 url
        logger.info("URL             : {}", request.getRequestURL().toString());
        // 鎵撳嵃鎻忚堪淇℃伅
        logger.info("Description     : {}", methodDescription);
        // 鎵撳嵃 Http method
        logger.info("HTTP Method     : {}", request.getMethod());
        // 鎵撳嵃璋冪敤 controller 鐨勫叏璺緞浠ュ強鎵ц鏂规硶
        logger.info("Class Method    : {}.{}", joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName());
        // 鎵撳嵃璇锋眰鐨� IP
        logger.info("IP              : {}", request.getRemoteAddr());
        // 鎵撳嵃璇锋眰鍏ュ弬
        logger.info("Request Headers : {}", getHeaderParams(request));
        logger.info("Request Args    : {}", joinPoint.getArgs());
    }

    /**
     * 鍦ㄥ垏鐐逛箣鍚庣粐鍏�
     * @throws Throwable
     */
    @After("apiLog()")
    public void doAfter() throws Throwable {

    }

    /**
     * 鐜粫
     * @param proceedingJoinPoint
     * @return
     * @throws Throwable
     */
    @Around("apiLog()")
    public Object doAround(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletResponse response = attributes.getResponse();
        long startTime = System.currentTimeMillis();
        Object result = proceedingJoinPoint.proceed();
        String businessRetObjStr = String.valueOf(result);
        // 鎵撳嵃涓氬姟鍑哄弬
        logger.info("Return Biz Result : {}", businessRetObjStr);
        // 鎵撳嵃鎵ц鑰楁椂
        logger.info("Time-Consuming : {} ms", System.currentTimeMillis() - startTime);
        // 鎺ュ彛缁撴潫鍚庢崲琛岋紝鏂逛究鍒嗗壊鏌ョ湅
        logger.info("=========================================== End ===========================================" + LINE_SEPARATOR);
        return businessRetObjStr;
    }

    /**
     * 鑾峰彇鍒囬潰娉ㄨВ鐨勬弿杩�-姝ｇ‘鏂瑰紡
     * @param joinPoint
     * @return
     * @throws Exception
     */
    public String getAspectLogDescription(JoinPoint joinPoint)
            throws Exception {
        Signature signature = joinPoint.getSignature();
        if (!(signature instanceof MethodSignature)) {
            throw new IllegalArgumentException("璇ユ敞瑙ｅ彧鑳界敤浜庢柟娉�");
        }
        try {
            ApiLog mockNotify = ((MethodSignature)joinPoint.getSignature()).getMethod().getAnnotation(ApiLog.class);
            return mockNotify.description();
        } catch (Exception e) {
            logger.info("鑾峰彇娉ㄨВ鎻忚堪寮傚父", e);
            return "";
        }
    }

    /**
     * 鑾峰彇澶村弬鏁�
     * @param request
     * @return
     */
    private String getHeaderParams(HttpServletRequest request) {
        StringBuilder sb = new StringBuilder();
        Enumeration<String> headNames = request.getHeaderNames();
        while(headNames.hasMoreElements()){
            String headName = headNames.nextElement();
            sb.append(headName).append(":").append(request.getHeader(headName)).append("||");
        }
        return sb.toString();
    }


}
